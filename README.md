
# run-if-changed

[![Build Status](https://gitlab.com/wwwouter/run-if-changed/badges/master/build.svg)](https://gitlab.com/wwwouter/run-if-changed/)
[![npm version](https://badge.fury.io/js/run-if-changed.svg)](https://badge.fury.io/js/run-if-changed)

[![npm](https://nodei.co/npm/run-if-changed.png?stars&downloads&downloadRank)](https://nodei.co/npm/run-if-changed.png?stars&downloads&downloadRank)

Executes a command when one or more files are changed.

## Installing

    npm install -g run-if-changed

## Using


Standard usage:

    run-if-changed -f **/* -c \"npm run buid\"

Different checksum file

    run-if-changed -f package.json -c \"npm install\" --checksumfile=.packagejson.checksum

## Options

    run-if-changed -f globPattern -c command [--checksumfile=.file.checksum] [--include=*.js] [--exclude=*.backup] [--outputfile=output.bin]

* globPattern: glob pattern to look for files.
* command: command to execute.
* --output: directory where output files will be written. (If empty, it will be in-place)
* --basedir: directory that is used as root for globPattern.
* --outputfile: if this file doesn't exist, the command is run.


## Test

npm test

## Develop

npm run dev:watch

## Update version

- commit changes (don't update version in package.json)
- update CHANGELOG.md and commit

    npm version major|minor|patch
    npm publish
    git push

## Tslint

rules
- https://github.com/palantir/tslint
- https://github.com/Microsoft/tslint-microsoft-contrib/


## [License](LICENSE)

Copyright (c) 2016 Wouter Mooij.

Licensed under the [MIT License](LICENSE).