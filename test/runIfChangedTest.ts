import * as tmp from 'tmp';
import * as fs from 'fs';
import * as os from 'os';
import * as path from 'path';
import runIfChanged from '../src/runIfChanged';
import { defaultChecksumFilename } from '../src/runIfChanged';
// import { IInputReplacement } from '../src/interfaces';
import * as chai from 'chai';
import * as chaiFs from 'chai-fs';
chai.use(chaiFs);
let assert = chai.assert;
let assertNotFileContent = (assert as any).notFileContent;
let assertFileContent = (assert as any).fileContent;
let assertPathExists = (assert as any).pathExists;
let assertNotPathExists = (assert as any).notPathExists;


let outputFileContents = os.platform() === 'win32' ? '1 \r\n' : '1\n';


describe('runIfChanged', () => {
    it('should run command when no old checksum file', () => {

        let workingDir = tmp.dirSync().name;
        let tempDir = tmp.dirSync().name;
        fs.writeFileSync(path.join(workingDir, 'file1.ext'), 'contents for checksum');
        const fileToBeCreatedPath = path.join(tempDir, 'test');

        return runIfChanged('*', workingDir, { command: 'echo 1 > ' + fileToBeCreatedPath })
            .then(() => {
                assertPathExists(path.join(workingDir, defaultChecksumFilename));
                assertFileContent(fileToBeCreatedPath, outputFileContents);

            });
    });
    it('should run command when different old checksum', () => {

        let workingDir = tmp.dirSync().name;
        let tempDir = tmp.dirSync().name;
        const checksumFilename = path.join(workingDir, defaultChecksumFilename);
        fs.writeFileSync(checksumFilename, 'differentchecksum');
        fs.writeFileSync(path.join(workingDir, 'file1.ext'), 'contents for checksum');
        const fileToBeCreatedPath = path.join(tempDir, 'test');

        return runIfChanged('*', workingDir, { command: 'echo 1 > ' + fileToBeCreatedPath })
            .then(() => {
                assertPathExists(checksumFilename);
                assertNotFileContent(checksumFilename, 'differentchecksum');
                assertFileContent(fileToBeCreatedPath, outputFileContents);

            });
    });

    it('should not run command when same old checksum', () => {

        let workingDir = tmp.dirSync().name;
        let tempDir = tmp.dirSync().name;
        const checksumFilename = path.join(workingDir, defaultChecksumFilename);
        fs.writeFileSync(checksumFilename, '07f14138cb3585ee397bd8b1d44901ee');
        fs.writeFileSync(path.join(workingDir, 'file1.ext'), 'contents for checksum');
        const fileToBeNotCreatedPath = path.join(tempDir, 'test');

        return runIfChanged('*', workingDir, { command: 'echo 1 > ' + fileToBeNotCreatedPath })
            .then(() => {
                assertNotPathExists(fileToBeNotCreatedPath);
            });
    });

    it('should work with checksumfile override', () => {

        let workingDir = tmp.dirSync().name;
        let tempDir = tmp.dirSync().name;
        const checksumFilename = path.join(workingDir, 'different.checksum');
        fs.writeFileSync(checksumFilename, 'oldchecksum');
        fs.writeFileSync(path.join(workingDir, 'file1.ext'), 'contents for checksum');
        const fileToBeCreatedPath = path.join(tempDir, 'test');

        return runIfChanged('*', workingDir, { command: 'echo 1 > ' + fileToBeCreatedPath, checksumFile: 'different.checksum' })
            .then(() => {
                assertPathExists(checksumFilename);
                assertNotFileContent(checksumFilename, 'oldchecksum');
                assertFileContent(fileToBeCreatedPath, outputFileContents);

            });
    });


    it('should run work for one file', () => {

        let workingDir = tmp.dirSync().name;
        let tempDir = tmp.dirSync().name;
        fs.writeFileSync(path.join(workingDir, 'file1.ext'), 'contents for checksum');
        const fileToBeCreatedPath = path.join(tempDir, 'test');

        return runIfChanged('file1.ext', workingDir, { command: 'echo 1 > ' + fileToBeCreatedPath })
            .then(() => {
                assertPathExists(path.join(workingDir, defaultChecksumFilename));
                assertFileContent(fileToBeCreatedPath, outputFileContents);

            });
    });


    it('should run command when missing output file', () => {

        let workingDir = tmp.dirSync().name;
        let tempDir = tmp.dirSync().name;
        const fileToBeCreatedPath = path.join(tempDir, 'test');

        return runIfChanged('*', workingDir, { command: 'echo 1 > ' + fileToBeCreatedPath, outputFile: 'doesnotexist' })
            .then(() => {
                assertFileContent(fileToBeCreatedPath.trim(), outputFileContents);

            });
    });

});