import * as Promise from 'bluebird';
import { IOptions } from './interfaces';
export default class Checksummer {
    private includes;
    private excludes;
    constructor(options?: IOptions);
    getMd5(globPattern: string, folderPath: string, options?: IOptions): Promise<string>;
    getFilenames(globPattern: string, folderPath: string): string[];
    private getMd5FromText;
    private includeFile;
}
