import * as execshell from 'exec-sh';
import * as fs from 'fs';
import { IOptions } from './interfaces';
import Checksummer from './Checksummer';
import * as path from 'path';

export const defaultChecksumFilename = '.runifchanged.checksum';

export function execshellPromise(command: string, options: any): Promise<void> {
    let promise = new Promise<void>((resolve, reject) => {
        execshell(command, options, (error: any, stdout: any, stderr: any) => {
            if (error) {
                return reject(error);
            }
            return resolve();
        });
    });
    return promise;
}

export default async function runIfChanged(globPattern: string, baseDir: string, options: IOptions) {
    let checksumFilename = path.join(baseDir, options.checksumFile || defaultChecksumFilename);

    let checksummer = new Checksummer(options);

    if (options.outputFile && fs.existsSync(options.outputFile) === false) {
        await execshellPromise(options.command, undefined);
    } else {
        let filesChecksum = await checksummer.getMd5(globPattern, baseDir, options);

        if (fs.existsSync(checksumFilename) === false) {
            await execshellPromise(options.command, undefined);
        } else {
            let oldChecksum = fs.readFileSync(checksumFilename).toString();
            if (filesChecksum !== oldChecksum) {
                await execshellPromise(options.command, undefined);
            }
        }

        fs.writeFileSync(checksumFilename, filesChecksum);
    }
}
